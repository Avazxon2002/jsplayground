// let a = 2;
// let x = 1 + (a *= 2);    // a *= 2 -> a = a * 2
// console.log(x, a);    // 5 4


// let a = 2;
// let x = 1 + (a * 2);    // a * 2
// console.log(x, a);    // 5 2


// let a = 1;
// a = -a
// console.log(a)   // -1

// console.log(5 % 2);   //1    % belgisi bo'lgandagi qoldiqni ko'rsatadi


// console.log(2 ** 2)   // 4    ** kvadrad belgisi ** dan keyin kelgan son esa nechi barobar ekani ya'ni:
// console.log(2 ** 3)   // 8    (** 2 = 2² / ** 3 = 2³ / ** 4 = 2⁴ vahokazo)

// console.log(4 ** (1 / 2));   // 2    1 / 2 bu 1ni yarmi degani...  1 / 3 bo'lsa 1ni 3dan 1 qismi bo'ladi
// console.log(8 ** (1 / 3));   // 2

// console.log('1' + 2 + 2);   // bu degani string bor bo'lsa raqam har doim stirnggda qo'shiladi

// console.log(6 - '2');   // 4   chunki stringdan stringdi ayirib bo'lib ko'paytibi bo'lmedi shunichun 2ni raqam qilib oladi

// console.log('6' / '2');   // 3


// function start() {
//     for (var i = 0; i < 5; i++) {
//         console.log(i)
//     }
//     console.log(i)
// }
// start()


// var age = 25;
// var age = 26;
// console.log(age)


// let apples = "2";
// let oranges = "3";
// console.log(+apples + +oranges);   // 5  Raqamli Stirng oldida + bo'lsa u raqamga aylanadi


// let a = 1;
// let b = 2;
// let c = 3 - (a = b + 1);   // a = b + 1  ->  a = 2 + 1  ->  a o'zgaruvchiligi uchun ani qiymat 3ga aylanadi
// console.log(a)   // 3
// console.log(c)   // 0


// let a, b, c;
// a = b = c = 2 + 2;
// console.log(a)   // 4
// console.log(b)   // 4
// console.log(c)   // 4
// a, b, c o'zgaruvchiligi uchun barchasini qiymati o'zgaradi


// let a = (1 + 2, 3 + 4);   // betta qiymat 2taligichun 1chisi o'qiladi va natijasi tashlanib 2-si o'qilib natija sifatida qaytarilinadi
// console.log(a);   // 7

// <>= belgilar boolean
// console.log(2 > 1);   //true (correct)   2 1dan kotta dedim rostadn kotta
// console.log(2 == 1);   //false (wrong)   2 1ga teng dedim noto'g'ri
// console.log(2 != 1);   //true (correct)   2 1dan kottamas dedim to'g'ri


// if else


// let login
// let message;
// if (login == 'Employee') {
//   message = 'Hello';
// } else if (login == 'Director') {
//   message = 'Greetings';
// } else if (login == '') {
//   message = 'No login';
// } else {
//   message = '';
// }

// let login = "";
// let message = (login == "Employee") ? "Hello" :
//     (login == "Director") ? "Greetings" :
//         (login == "") ? "No Login" :
//             "";
// console.log(message)


// kichigini topish

// let a = 7;
// let b = 15;
// let c = 2;
//
// if (a < b && a < c) {
//     console.log("a kichik")
// } else if (b < a && b < c) {
//     console.log("b kichik")
// } else {
//     console.log("c kichik")
// }


